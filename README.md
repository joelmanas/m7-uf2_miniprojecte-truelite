PHP M7 UF2
Angel Raul Cortes, Joel Mañas

# Proyecto Final Laravel
### Envío de Correos y Scheduling

## OBJETIVOS
- Enviar los detalles de una película del catálogo / Enviar un detalle del catálogo con las cinco películas añadidas recientemente..
- Programar una tarea que se ejecute periódicamente. En este caso, ejecutaremos el envío de mail de manera periódica.
## REGISTRO
### .env [Modificado]
Se ha modificado el archivo de las variables de entorno para definir los datos de conexión con el servicio de envío de correos:
```
MAIL_MAILER=smtp // Protocolo de envío de correo simple
MAIL_HOST=smtp.gmail.com // Servidor de correo utilizado
MAIL_PORT=587 // Puerto de salida con nivel de restricción bajo
MAIL_USERNAME=videoclubm7 // Usuario del servidor de correo
MAIL_PASSWORD=contraseña // Contraseña de inicio de sesión en el servidor de correo
MAIL_ENCRYPTION=tls // Transport Layer Security - Capa de abstracción y encriptación de la información traspasada por email
MAIL_FROM_ADDRESS=videoclubm7@gmail.com // Dirección de correo de envío
MAIL_FROM_NAME="True Elite" // Nombre de referencia de los correos
```

## Controllers
### MailController [Añadido]
Este archivo es necesario para controlar la llamada de métodos relacionados con preparar un email y enviarlo.
Estos métodos son movieCatalog(), el cual recoge todos los email de los usuarios registrados en la plataforma y los utiliza para enviarles un email con las últimas 5 películas añadidas. 
newMovie() es el otro método. Se encarga de recoger los emails de los usuarios registrados en la plataforma y les envía un email. Este email envía datos de la última película añadida por cualquier usuario en la plataforma.

### CatalogController [Modificado]
Se modificó el archivo con el fin de aplicar una nueva funcionalidad al notificar a los usuarios registrados de la página por correo cada vez que se introducía una nueva película a la base de datos, con la información de la misma.
Estos cambios de reflejan en el método postCreate()

## Console & Commands
### EmailCron (Command) [Añadido]
Se define el nuevo comando que se encarga de llamar a la función movieCatalog() del controlador MailController, que recoge las últimas 5 películas registradas en la base de datos y envía un resumen por correo a todos los usuarios registrados.

### Kernel [Modificado]
Se define el nuevo comando, email:cron, para que pueda ser ejecutado desde la línea de comandos de Artisan.
Conlleva modificar el vector $commands y añadir la clase Command a la que hace referencia.
Definir en el método schedule() el comando a ser ejecutado periódicamente.

## Vistas
### singlemail [Modificado]
Plantilla para mostrar los datos de una película en específico. Esta vista

### catalogmail [Modificado]
Plantilla para mostrar resumen de las últimas 5 películas.

## Routes
### web [Modificado]
Añadidas las rutas para la ejecución de los métodos newMovie() y movieCatalog() del controlador MailController.