@extends('layouts.master')

@section('content')

<div class="row">

	<div class="col-sm-4">

		{{-- TODO: Imagen de la película --}}
		<img src="{{ $pelicula->poster }}" style="height:200px"/>

	</div>
	<div class="col-sm-8">

		{{-- TODO: Datos de la película --}}
		<dl>
			<dt>
				{{ $pelicula->title }}
				@if($pelicula->rented)
					<span style="font-weight: 400">- No disponible</span>
				@else
					<span style="font-weight: 400">- Disponible</span>
				@endif
			</dt>
			<dd>{{ $pelicula->year }}</dd>
			<dd>{{ $pelicula->director }}</dd>
			<dd>{{ $pelicula->synopsis }}</dd>
		</dl>
		<div style="display: flex;">
			@if($pelicula->rented)
			<p><input class="btn btn-success" style="margin: 0 3px;" type="submit" value="Devolver" name="devolver"></p>
			@else
			<p><input class="btn btn-info" style="margin: 0 3px;" type="submit" value="Alquilar" name="alquilar"></p>
			@endif
			<form action="{{ url('/catalog/edit/'.$pelicula->id) }}">
				<p><input class="btn btn-warning" style="margin: 0 3px;" type="submit" value="Editar"></p>
			</form>
			<form action="{{ url('/catalog') }}">
				<p><input class="btn btn-default" style="margin: 0 3px;" type="submit" value="Volver"></p>
			<form>
		</div>


	</div>

</div>

@endsection