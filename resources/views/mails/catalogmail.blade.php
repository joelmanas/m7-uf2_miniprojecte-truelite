<head>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Cinzel:wght@400;700&display=swap');
		* {
		font-family: Cinzel;
		}

		ul {
		list-style: none;
		margin: 0;
		padding: 0;
		}

		#catalog {
		display: flex;
		flex-wrap: wrap;
		justify-content: center;
		}

		#catalog ul {
		flex: 0 1 300px;
		margin: 10px;
		}
	</style>
</head>
<body>
<h1>Hola, {{ $name }}</h1>

<p>Este es el listado de las últimas 5 películas añadidas en nuestro catálogo:</p>

<div id="catalog">

@foreach ( $peliculas as $pelicula)
<ul>
    <li><strong>{{ $pelicula->title }}</strong><br>
      ({{ $pelicula->year }})</li>
    <li>Del director {{ $pelicula->director }}</li>
    <li>
        <img src="{{ $pelicula->poster }}" style="height:200px"/>
    </li>
    <li>{{ $pelicula->synopsis }}</li>
</ul>
@endforeach

</div>

</body>