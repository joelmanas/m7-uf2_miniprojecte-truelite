<?php

namespace App\Repositories;

use Mail;
use App\User;
use App\Movie;
use Illuminate\Support\Facades\Auth;

class MailRepository
{
	public static function newMovie($pelicula) {

		$users = User::all();

		foreach( $users as $user){
		
			$data = array('name'=> $user->name, 'pelicula' => $pelicula);
	
			Mail::send('mails.singlemail', $data, function($message) use ($user){ //??????????????
				$message->to($user->email, $user->name)->subject
					('Se ha añadido una nueva pelicula al catalogo!');
			});
		}

		return redirect()->action('CatalogController@getIndex');
	}

	public static function movieCatalog(){

		$users = User::all();
		$movies = Movie::orderBy('created_at', 'DESC')->take(5)->get();

		
	
		foreach( $users as $user ){

			$data = array('name'=> $user->name, 'peliculas' => $movies);
			
			Mail::send('mails.catalogmail', $data, function($message) use ($user){ //??????????????
				$message->to($user->email, $user->name)->subject
					('¡El listado semanal de películas está aquí!');
			});
		}
	}
}
