<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;
use App\Repositories\MailRepository;

class CatalogController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getIndex()
	{
		return view('catalog.index', ['arrayPeliculas' => Movie::all()]);
	}

	public function getShow($id)
	{
		return view('catalog.show', ['pelicula' => Movie::findOrFail($id)]);
	}

	public function getCreate()
	{
		return view('catalog.create');
	}

	public function getEdit($id)
	{
		return view('catalog.edit', ['pelicula' => Movie::findOrFail($id)]);
	}

	public function postCreate(Request $request)
	{
		$p = new Movie;
		$p->title = $request->input('title');
		$p->year = $request->input('year');
		$p->director = $request->input('director');
		$p->poster = $request->input('poster');
		$p->synopsis = $request->input('synopsis');
		$p->save();

		return MailRepository::newMovie($p);
	}

	public function putEdit(Request $request, $id)
	{
		$p = Movie::findOrFail($id);
		$p->title = $request->input('title');
		$p->year = $request->input('year');
		$p->director = $request->input('director');
		$p->poster = $request->input('poster');
		$p->synopsis = $request->input('synopsis');
		$p->save();

		return redirect()->action('CatalogController@getShow', ['id' => $id]);
	}
}
